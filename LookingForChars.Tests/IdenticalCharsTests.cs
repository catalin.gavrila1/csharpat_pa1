using NUnit.Framework;

// ReSharper disable StringLiteralTypo
namespace LookingForChars.Tests
{
    [TestFixture]
    public class IdenticalCharsTests
    {
        [Test]
        public void GetConsecutiveIdenticalLatinCharsMaxCount_StrParameterIsNull_ThrowsException()
        {
            // Act
            Assert.Throws<ArgumentNullException>(() => CharsCounter.GetConsecutiveIdenticalLatinCharsMaxCount(null));
        }

        [Test]
        public void GetConsecutiveIdenticalDigitsMaxCount_StrParameterIsNull_ThrowsException()
        {
            // Act
            Assert.Throws<ArgumentNullException>(() => CharsCounter.GetConsecutiveIdenticalDigitsMaxCount(null));
        }

        [TestCase("", ExpectedResult = 0)]
        [TestCase(",./ ;'[]", ExpectedResult = 0)]
        [TestCase("1234567890", ExpectedResult = 0)]
        [TestCase("abcdef", ExpectedResult = 0)]
        [TestCase("aAbBcCdDeEfF", ExpectedResult = 0)]
        [TestCase("aabbccddeeff", ExpectedResult = 2)]
        [TestCase("abbbcdddefaaaabbcdef", ExpectedResult = 4)]
        [TestCase("aaaaaaaa", ExpectedResult = 8)]
        [TestCase("AAAAAAAa", ExpectedResult = 7)]
        public int GetConsecutiveIdenticalLatinCharsMaxCount_ParametersAreValid_ReturnsResult(string str)
        {
            // Act
            return CharsCounter.GetConsecutiveIdenticalLatinCharsMaxCount(str);
        }

        [TestCase("", ExpectedResult = 0)]
        [TestCase(",./ ;'[]", ExpectedResult = 0)]
        [TestCase("abcdef", ExpectedResult = 0)]
        [TestCase("1234567890", ExpectedResult = 0)]
        [TestCase("aa11ccddeeff", ExpectedResult = 2)]
        [TestCase("ab00c111ef9999bbcdef", ExpectedResult = 4)]
        [TestCase("11111111", ExpectedResult = 8)]
        public int GetConsecutiveIdenticalDigitsMaxCount_ParametersAreValid_ReturnsResult(string str)
        {
            // Act
            return CharsCounter.GetConsecutiveIdenticalDigitsMaxCount(str);
        }
    }
}