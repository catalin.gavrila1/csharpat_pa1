﻿using System;

namespace LookingForChars
{
    public static class CharsCounter
    {
        /// <summary>
        /// Searches a string for all characters that are in <see cref="Array" />, and returns the number of occurrences of all characters.
        /// </summary>
        /// <param name="str">String to search.</param>
        /// <param name="chars">One-dimensional, zero-based <see cref="Array"/> that contains characters to search for.</param>
        /// <returns>The number of occurrences of all characters.</returns>
        public static int GetCharsCount(string? str, char[]? chars)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            if (chars.Length <= 0)
            {
                return 0;
            }

            if (str.Length <= 0)
            {
                return 0;
            }

            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                for (int j = 0; j < chars.Length; j++)
                {
                    if (str[i] == chars[j])
                    {
                        count++;
                        break;
                    }
                }
            }

            return count;
        }

        /// <summary>
        /// Searches a string for all characters that are in <see cref="Array" />, and returns the number of occurrences of all characters within the range of elements in the <see cref="string"/> that starts at the specified index and ends at the specified index.
        /// </summary>
        /// <param name="str">String to search.</param>
        /// <param name="chars">One-dimensional, zero-based <see cref="Array"/> that contains characters to search for.</param>
        /// <param name="startIndex">A zero-based starting index of the search.</param>
        /// <param name="endIndex">A zero-based ending index of the search.</param>
        /// <returns>The number of occurrences of all characters within the specified range of elements in the <see cref="string"/>.</returns>
        public static int GetCharsCount(string? str, char[]? chars, int startIndex, int endIndex)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            if (chars.Length <= 0)
            {
                return 0;
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index is negative");
            }

            if (endIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex), "End index is negative");
            }

            if (startIndex >= str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index is greater than string length");
            }

            if (endIndex >= str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex), "End index is greater than string length");
            }

            if (startIndex > endIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index is greater than end index");
            }

            if (str.Length <= 0)
            {
                return 0;
            }

            int count = 0, i = startIndex;
            while (i <= endIndex)
            {
                int j = 0;
                while (j < chars.Length)
                {
                    if (str[i] == chars[j])
                    {
                        count++;
                        break;
                    }

                    j++;
                }

                i++;
            }

            return count;
        }

        /// <summary>
        /// Searches a string for a limited number of characters that are in <see cref="Array" />, and returns the number of occurrences of all characters within the range of elements in the <see cref="string"/> that starts at the specified index and ends at the specified index.
        /// </summary>
        /// <param name="str">String to search.</param>
        /// <param name="chars">One-dimensional, zero-based <see cref="Array"/> that contains characters to search for.</param>
        /// <param name="startIndex">A zero-based starting index of the search.</param>
        /// <param name="endIndex">A zero-based ending index of the search.</param>
        /// <param name="limit">A maximum number of characters to search.</param>
        /// <returns>The limited number of occurrences of characters to search for within the specified range of elements in the <see cref="string"/>.</returns>
        public static int GetCharsCount(string? str, char[]? chars, int startIndex, int endIndex, int limit)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars));
            }

            if (chars.Length <= 0)
            {
                return 0;
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index is negative");
            }

            if (endIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex), "End index is negative");
            }

            if (startIndex >= str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index is greater than string length");
            }

            if (endIndex >= str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex), "End index is greater than string length");
            }

            if (startIndex > endIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index is greater than end index");
            }

            if (limit < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(limit), "Limit is negative");
            }

            if (str.Length <= 0)
            {
                return 0;
            }

            if (limit == 0)
            {
                return 0;
            }

            int count = 0, i = startIndex;
            do
            {
                int j = 0;
                do
                {
                    if (str[i] == chars[j])
                    {
                        count++;
                        break;
                    }

                    j++;
                }
                while (j < chars.Length);

                if (count == limit)
                {
                    break;
                }

                i++;
            }
            while (i <= endIndex);

            return count;
        }

        public static bool IsLatinLetter(char c)
        {
            return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
        }

        public static int GetConsecutiveIdenticalLatinCharsMaxCount(string? str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (str.Length <= 0)
            {
                return 0;
            }

            int count = 0, maxCount = 0;
            for (int i = 1; i < str.Length; i++)
            {
                if (IsLatinLetter(str[i]) && str[i] == str[i - 1])
                {
                    count++;
                }
                else
                {
                    if (maxCount < count)
                    {
                        maxCount = count;
                    }

                    count = 0;
                }
            }

            if (maxCount == 0)
            {
                maxCount = count;
            }

            return maxCount > 0 ? maxCount + 1 : 0;
        }

        public static int GetConsecutiveIdenticalDigitsMaxCount(string? str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (str.Length <= 0)
            {
                return 0;
            }

            int count = 0, maxCount = 0;
            for (int i = 1; i < str.Length; i++)
            {
                if (char.IsDigit(str[i]) && str[i] == str[i - 1])
                {
                    count++;
                }
                else
                {
                    if (maxCount < count)
                    {
                        maxCount = count;
                    }

                    count = 0;
                }
            }

            if (maxCount == 0)
            {
                maxCount = count;
            }

            return maxCount > 0 ? maxCount + 1 : 0;
        }
    }
}
