# Unit Testing Task

Please find the Task description below and provide the link to your solution in the Answer field:


## What should be done

Modify the application from the first task of the Basic of .NET framework and C # by separating the functionality of counting the maximum number of unequal consecutive characters in a string into a separate method. Extend functionality by adding two more methods:
determining the maximum number of consecutive identical letters of the Latin alphabet in a line
determining the maximum number of consecutive identical digits

For each method, write Unit Tests (use the NUnit, XUnit or MSTest framework is your choice; follow AAA and FIRST). When creating tests, pay special attention to equivalence classes.

